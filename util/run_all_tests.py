import run_test as rt

tests_to_run = ["llog_test", "model_test", "persistence_test"]

if __name__ == "__main__":
    rets = []
    exit_code = 0

    for test in tests_to_run:
        rets.append(rt.run_test(test))

    for i in range(len(rets)):
        print(f"Test {tests_to_run[i]} returned {rets[i]}")

        if rets[i] != 0:
            exit_code = 1

    exit(exit_code)
