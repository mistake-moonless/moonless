import sys
import subprocess
import os

if __name__ == "__main__":
    # install pre-commit package
    subprocess.check_call([sys.executable, "-m", "pip", "install", "pre-commit"])

    # setup pre-commit hooks for the repo
    subprocess.check_call(["pre-commit", "install"])

    # generate cmake files
    os.system(f"mkdir -p build && cd build && cmake .. -Wno-dev && cd ..")

    # create symlink for compilation database
    os.symlink(f"{os.getcwd()}/build/compile_commands.json", f"{os.getcwd()}/compile_commands.json")
