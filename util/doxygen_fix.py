import os
import sys


def fix_path(start_path: str, str_to_find: str, replce_with: str):
    for root, dirs, files in os.walk(start_path):
        for file_name in files:
            if "html" not in file_name:
                continue

            full_path = os.path.join(root, file_name)
            with open(full_path, "r+") as f:
                filedata = f.read()
                filedata = filedata.replace(str_to_find, replce_with)
                f.seek(0)
                f.write(filedata)


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: doxygen_img_path_fix.py <start_path> <str_to_find> <replce_with>")
        sys.exit(1)

    # make doesn't like '<' and '>' so using [] instead
    sys.argv[2] = sys.argv[2].replace("[", "<").replace("]", ">")
    sys.argv[3] = sys.argv[3].replace("[", "<").replace("]", ">")
    fix_path(sys.argv[1], sys.argv[2], sys.argv[3])
