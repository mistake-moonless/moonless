import os
import sys
from typing import Optional


def find_file(file_name: str, path: str) -> Optional[str]:
    for root, dirs, files in os.walk(path):
        if file_name in files:
            return os.path.join(root, file_name)
    return None


def run_test(test_name: str) -> int:
    '''
    Run a test.
    test_name: name of the test to run
    return: 0 if test passed
            -1 if cmake failed
            -2 if test is not found
            everything else if test failed
    '''
    file_path = find_file(test_name, ".")

    if file_path is None:
        file_path = find_file(test_name, "./build")

        if file_path is None:
            return -2

    return os.system(file_path)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: run_test.py <test_name> [<test_name> ...]")
        sys.exit(1)

    for test_name in sys.argv[1:]:
        ret = run_test(test_name)

        if ret == -1:
            print("CMake failed")
            sys.exit(ret)
        elif ret == -2:
            print("Test not found")
            sys.exit(ret)
        elif ret != 0:
            print("Test failed")
            sys.exit(ret)
