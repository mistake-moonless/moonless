# Cli

If you opent the cli window, you see the logo of the project and 3 options:
1. ```q```: Qiting from the program.
2. ```a```: Add a new session.
3. ```r```: Remove a session.

![cli_starting_screen](cli_images/0_starting_screen.png)

## 1. Add a new session

If you choose the second option, you need to add a session group name(new or existing), a seesion name, an ip , a port, the username to the session, and the connectiontype(ssh or telnet).

Here is a valid session:

![cli_save_session](cli_images/1_save_session.png)

And here is, what it looks like, if you add a new session:

![cli_with_saved_session](cli_images/2_cli_with_saved_session.png)

## 2. Remove a session

If you want to remove a session, you need to add the name of the session group, whereout you want to remove the session, and the name of the session.

Here is the removing process:

![cli_delete_session](cli_images/3_delete_session.png)

## 3. Start a saved session

If you want to start a saved session, you need to add the number of the session.

Here is an example:

![cli_start_session](cli_images/4_start_session.png)

# Gui

Our program also have a gui version. In this version you can add new session, edit session, add new session group, delete session group, delete session, and start a saved session.

Here is the starting screen:

1. Add new session
2. Add nem session group
3. Edit session
4. Delete session/session group

![gui_starting_screen](gui_images/0_empty_start.png)

## 1. Add a new session

In a dialog window you can add the necessary datas to add a new session. In the gui version you can add the session as ungrouped.

Here is an empty dialog window:

![empty_create_dialog](gui_images/1_create_dialog.png)

And here is a filled dialog window:

![filled_create_dialog](gui_images/2_create_dialog_filled.png)

And here is, what it looks like, after you aded an ungrouped session:

![create_dialog_result](gui_images/3_create_result.png)

## 2. Edit session

In the gui version you also can edit the saved session in another dialog window.

Here is the editing dialog window:

![edit_dialog](gui_images/4_edit_dialog.png)

And here is what it looks like, after you edited the session:

![edit_dialog_result](gui_images/5_edit_result.png)

## 3. Create session group

You can create empty session groups in this version. Later you can choose, wich one do you want to add a session, or leave ungrouped.

Here is a session creating dialog window:

![create_group_dialog](gui_images/6_create_group_dialog.png)

And here is what it looks like, after you created an empty session group

![create_group_dialog_result](gui_images/7_create_group_result.png)

## 4. Delete session group

Here is an example with a session group with a session, and an ungrouped session:

![before_delete_group](gui_images/8_before_delete_group.png)

After you clicked the session group, what you want to delete, you need to click to delete icon. Before deleting a session group, it appears a dialog window to confirm the process.

![delete_group_dialog](gui_images/9_delete_group_dialog.png)

And here is what it looks like, after deleted the session group:

![delete_group_result](gui_images/10_delete_group_result.png)

## 5. Delete session

Deleting a session works simular to deleting a session group.
Before deleting a session, it also appears a dialog window to confirm the process.

![delete_session_dialog](gui_images/11_delete_session_dialog.png)

And here is what it looks like, after deleted the session:

![delete_session_result](gui_images/12_delete_session_result.png)

## 6. Start a saved session

To start a saved session you need to double click to the session name, and it opens a new window, where you can see the same result as strating in cli.

Here is an example:

![start_sessin_result](gui_images/13_start_session_result.png)