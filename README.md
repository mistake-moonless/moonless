![logo](docs/logo_wide.png)

# moonless

moonless or <ins>**m**ulti platf**o**rm **o**pe**n**source te**l**n**e**t and **ss**h manager</ins> is a foss, cross-platform alternative to MTPutty.

## Documentation

[https://mistake-moonless.gitlab.io/moonless](https://mistake-moonless.gitlab.io/moonless/)

## Dependencies

### Pre-commit hooks:

- python3
- pip3
- _pre-commit_ pip pkg

### Compilation:

- cmake
- g++/clang
- clang-tidy
- clang-format
- python3
- doxygen

#### GUI dependencies

- Qt6
- Konsole

The project was developed with GCC 11.2 running on Ubuntu 22.04

## Setup

1. Install dependencies
2. `git clone --recursive https://gitlab.com/mistake-moonless/moonless.git`
3. `cd moonless`
4. `python3 util/setup_precommit.py`

## Building/Running tests

```bash
# run all tests
[moonless]$ python3 util/run_all_tests.py
# run a specific test
[moonless]$ python3 util/run_test.py <test_name>
```

## Conventions

### Coding convention:

- **[LLVM](https://llvm.org/docs/CodingStandards.html)** but with **4 spaces** for indentation
- CPP version: C++17

### Commit convetions:

- Commit message should be at least one line.
- Commit message title should be less then 50 characters.
- Use `git commit --amend` if possible.
- Merges to master should be squashed if the commits doesn't work by themselves.
- **Pre-commit hooks** should be set up before committing.

## License
This project is licensed under the Apache License Version 2.0 - see the [Apache License Version 2.0](LICENSE) file for details.
