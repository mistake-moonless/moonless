#ifndef GUI_H
#define GUI_H

#include "persistence.h"
#include "selection_data.h"
#include <QLabel>
#include <QMainWindow>
#include <QStandardItemModel>
#include <QWidget>
#include <qabstractitemmodel.h>
#include <qtmetamacros.h>
#include <qtreeview.h>
#include <vector>

class Gui : public QMainWindow {
    Q_OBJECT

  public:
    Gui(QWidget *parent = nullptr);
    ~Gui();

  private:
    Persistence persistence;
    QStandardItemModel *sessionsData;
    QTreeView *sessionsTreeView;
    QTabWidget *consoleWidget;
    void buildDataTree();
    void initDataTree();
    void initToolbar();
    std::string generateConnectionString(const nlohmann::json &connection);
    std::vector<std::string> getGroupNames();
    QPixmap coloredIcon(const char *res);
    SelectionData getSelected();

  private slots:
    void onTreeClicked(QModelIndex id);
    void newSession();
    void newGroup();
    void editItem();
    void deleteItem();
};
#endif // GUI_H
