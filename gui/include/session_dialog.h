#ifndef SESSIONDIALOG_H
#define SESSIONDIALOG_H

#include "connection_type.h"
#include <QComboBox>
#include <QDialog>
#include <qcombobox.h>
#include <qlineedit.h>
#include <qnamespace.h>
#include <qtmetamacros.h>
#include <qwidget.h>
#include <string>
#include <vector>

/**
 * @brief Custom QDialog class for creating and editing new Sessions
 * 
 */
class SessionDialog : public QDialog {

  private:
    QLineEdit *nameField = new QLineEdit(this);
    QComboBox *groupCombo = new QComboBox(this);
    QComboBox *typeCombo = new QComboBox(this);
    QLineEdit *ipField = new QLineEdit(this);
    QLineEdit *portField = new QLineEdit(this);
    QLineEdit *usernameField = new QLineEdit(this);

  public:
    SessionDialog(QWidget *parent = nullptr, bool editMode = false,
                  Qt::WindowFlags f = Qt::WindowFlags());

    int exec() override;

    std::string getName() { return nameField->text().toStdString(); }

    void setName(const std::string &name) { nameField->setText(name.c_str()); }

    std::string getIP() { return ipField->text().toStdString(); }

    void setIP(const std::string &ip) { ipField->setText(ip.c_str()); }

    std::string getPort() { return portField->text().toStdString(); }

    void setPort(const std::string &port) { portField->setText(port.c_str()); }

    std::string getUsername() { return usernameField->text().toStdString(); }

    void setUsername(const std::string &username) {
        usernameField->setText(username.c_str());
    }

    ConnectionType getType() {
        return ConnectionType(typeCombo->currentIndex());
    }

    void setType(ConnectionType type) { typeCombo->setCurrentIndex(type); }

    std::string getGroup() { return groupCombo->currentText().toStdString(); }

    void addGroups(std::vector<std::string> groupNames);

    void setGroup(const std::string &groupName) {
        groupCombo->setCurrentText(groupName.c_str());
    }

  private slots:
    void onCancel();
    void onSave();
};

#endif