#ifndef SELECTION_DATA_H
#define SELECTION_DATA_H

#include <qabstractitemmodel.h>
#include <string>

enum SelectionType { SESSION, GROUP };

/**
 * @brief Data structure for storing information about the currently selected item
 * 
 */
struct SelectionData {
    std::string parentName;
    std::string itemName;
    SelectionType itemType;
    QModelIndex index;
};

#endif