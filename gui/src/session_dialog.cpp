#include "session_dialog.h"
#include <QDialog>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <qboxlayout.h>
#include <qdialog.h>
#include <qdialogbuttonbox.h>

SessionDialog::SessionDialog(QWidget *parent, bool editMode, Qt::WindowFlags f)
    : QDialog(parent, f) {
    // setMinimumSize(200,300);
    setWindowTitle(editMode ? "Edit session" : "Create new session");
    auto mainLayout = new QVBoxLayout(this);

    auto nameLayout = new QHBoxLayout();
    nameLayout->addWidget(new QLabel("Name", this));
    nameLayout->addWidget(nameField);
    mainLayout->addLayout(nameLayout);

    auto groupLayout = new QHBoxLayout();
    groupLayout->addWidget(new QLabel("Group", this));
    groupLayout->addWidget(groupCombo);
    mainLayout->addLayout(groupLayout);

    auto typeLayout = new QHBoxLayout();
    typeLayout->addWidget(new QLabel("Type"));
    typeLayout->addWidget(typeCombo);
    typeCombo->insertItem(1, "SSH");
    typeCombo->insertItem(0, "TELNET");
    typeCombo->activated(1);
    mainLayout->addLayout(typeLayout);

    auto ipLayout = new QHBoxLayout();
    ipLayout->addWidget(new QLabel("IP Address"));
    ipLayout->addWidget(ipField);
    ipLayout->addWidget(new QLabel("Port"));
    ipLayout->addWidget(portField);
    mainLayout->addLayout(ipLayout);

    auto userLayout = new QHBoxLayout();
    userLayout->addWidget(new QLabel("Username"));
    userLayout->addWidget(usernameField);
    mainLayout->addLayout(userLayout);

    auto buttons =
        new QDialogButtonBox(QDialogButtonBox::Save | QDialogButtonBox::Cancel);
    mainLayout->addWidget(buttons);
    connect(buttons, &QDialogButtonBox::accepted, this, &SessionDialog::onSave);
    connect(buttons, &QDialogButtonBox::rejected, this,
            &SessionDialog::onCancel);
}
/**
 * @brief Checks if any SessionGroups were set before showing the dialog, if not, adds an "Ungrouped" entry, then shows the dialog
 * 
 * @return int 
 */
int SessionDialog::exec() {
    if (groupCombo->count() == 0) {
        groupCombo->insertItem(0, "Ungrouped");
    }
    return QDialog::exec();
}

void SessionDialog::onSave() {
    // TODO: check input
    accept();
};
void SessionDialog::onCancel() { reject(); };

/**
 * @brief Appends the recieved group names to the SessionGroup selector
 * 
 * @param groupNames 
 */
void SessionDialog::addGroups(std::vector<std::string> groupNames) {
    int itemCount = groupCombo->count();
    for (auto name : groupNames) {
        groupCombo->insertItem(itemCount++, name.c_str());
    }
}