#include "gui.h"
#include "connection_type.h"
#include "nlohmann/json.hpp"
#include "session_dialog.h"
#include <cstdlib>
#include <qabstractitemview.h>
#include <qaction.h>
#include <qapplication.h>
#include <qdialog.h>
#include <qicon.h>
#include <qinputdialog.h>
#include <qlineedit.h>
#include <qmainwindow.h>
#include <qmessagebox.h>
#include <qnamespace.h>
#include <qpixmap.h>
#include <qstandarditemmodel.h>
#include <qtoolbar.h>
#include <sstream>
#include <string>

static std::string getSaveFilePath() {
#if _WIN32
    return std::string(std::getenv("HOMEDRIVE")) +
           std::string(std::getenv("HOMEPATH")) + "\\.moonless_gui.json";
#elif __linux__
    return std::string(std::getenv("HOME")) + "/.moonless_gui.json";
#else
#error Not supported platform
#endif
}

Gui::Gui(QWidget *parent) : QMainWindow(parent) {
    persistence = Persistence(getSaveFilePath());
    setWindowTitle("Moonless");
    resize(300, 720);
    setWindowIcon(QIcon(":/icon.png"));

    initDataTree();
    initToolbar();
}

/**
 * @brief Initializes the display data structure from persistence, and sets up
 * the data view widget
 *
 */
void Gui::initDataTree() {
    sessionsTreeView = new QTreeView(this);
    sessionsTreeView->setHeaderHidden(true);
    sessionsTreeView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    sessionsData = new QStandardItemModel(this);

    buildDataTree();

    sessionsTreeView->setModel(sessionsData);

    sessionsTreeView->sortByColumn(1, Qt::SortOrder::AscendingOrder);
    sessionsTreeView->setSortingEnabled(true);

    connect(sessionsTreeView, &QTreeView::doubleClicked, this,
            &Gui::onTreeClicked);

    setCentralWidget(sessionsTreeView);
}

/**
 * @brief Builds the data structure from persistence
 *
 */
void Gui::buildDataTree() {
    // remove already existing data
    if (sessionsData->rowCount() > 0)
        sessionsData->clear();

    for (auto &group : persistence["data"].items()) {
        QStandardItem *groupItem = new QStandardItem(group.key().c_str());

        for (auto &connection : group.value().items()) {
            QStandardItem *connectionItem =
                new QStandardItem(connection.key().c_str());

            connectionItem->setData(
                generateConnectionString(connection.value()).c_str(),
                Qt::UserRole);

            groupItem->appendRow(connectionItem);
        }

        sessionsData->appendRow(groupItem);
    }
}

/**
 * @brief Generates a connection command string from connection json data
 *
 * @param connection
 * @return std::string
 */
std::string Gui::generateConnectionString(const nlohmann::json &connection) {
    std::stringstream connectionString;
    connectionString << "ssh " << connection["ip"] << " -p "
                     << connection["port"] << " -l " << connection["username"];
    return connectionString.str();
}

/**
 * @brief Initializes the main toolbar containing the action buttons
 *
 */
void Gui::initToolbar() {
    auto toolbar = new QToolBar(this);
    toolbar->setMovable(false);

    auto newSessionAction = new QAction("New session", toolbar);
    newSessionAction->setIcon(coloredIcon(":/create-file"));
    connect(newSessionAction, &QAction::triggered, this, &Gui::newSession);
    toolbar->addAction(newSessionAction);

    auto newGroupAction = new QAction("New group", toolbar);
    newGroupAction->setIcon(coloredIcon(":/create-folder"));
    connect(newGroupAction, &QAction::triggered, this, &Gui::newGroup);
    toolbar->addAction(newGroupAction);

    auto editItemAction = new QAction("Edit", toolbar);
    editItemAction->setIcon(coloredIcon(":/edit"));
    connect(editItemAction, &QAction::triggered, this, &Gui::editItem);
    toolbar->addAction(editItemAction);

    auto deleteItemAction = new QAction("Delete connection", toolbar);
    deleteItemAction->setIcon(coloredIcon(":/delete"));
    connect(deleteItemAction, &QAction::triggered, this, &Gui::deleteItem);
    toolbar->addAction(deleteItemAction);

    addToolBar(toolbar);
}

/**
 * @brief Adapts icon colors to the current OS colors
 *
 * @param res
 * @return QPixmap
 */
QPixmap Gui::coloredIcon(const char *res) {
    QPixmap in(res);
    QPixmap np = QPixmap(in.size());
    np.fill(QApplication::palette().text().color());
    np.setMask(in.createMaskFromColor(Qt::transparent));
    return np;
}

/**
 * @brief Event handler for the main TreeView, opens new sessions in a terminal
 *
 * @param id
 */
void Gui::onTreeClicked(QModelIndex id) {

    // root level node, these are SessionGroups
    if (!id.parent().isValid())
        return;

    system(("konsole --hold -e " +
            id.data(Qt::UserRole).toString().toStdString() + "&")
               .c_str());
}

/**
 * @brief Slot for creating new Sessions
 *
 */
void Gui::newSession() {
    auto selected = getSelected();
    SessionDialog dialog(this);
    dialog.addGroups(getGroupNames());

    // auto select group for the dialog based on currently selected item
    dialog.setGroup(selected.itemType == SelectionType::GROUP
                        ? selected.itemName
                        : selected.parentName);
    int result = dialog.exec();
    if (result == QDialog::Accepted) {
        // save retrieved data to persistence
        auto connection = nlohmann::json({});

        connection["type"] =
            dialog.getType() == ConnectionType::SSH ? "SSH" : "TELNET";
        connection["ip"] = dialog.getIP();
        connection["port"] = dialog.getPort();
        connection["username"] = dialog.getUsername();

        persistence["data"][dialog.getGroup()][dialog.getName()] = connection;

        // update view data model
        auto item = new QStandardItem(dialog.getName().c_str());
        auto parent_a = sessionsData->findItems(dialog.getGroup().c_str());
        QStandardItem *parent;
        if (parent_a.isEmpty()) {
            // create a new group if it couldn't be found
            persistence["data"][dialog.getGroup()] = nlohmann::json({});
            auto group = new QStandardItem(dialog.getGroup().c_str());
            sessionsData->appendRow(group);
            parent = group;
        } else {
            parent = parent_a[0];
        }
        parent->appendRow(item);
        sessionsData->setData(item->index(),
                              generateConnectionString(connection).c_str(),
                              Qt::UserRole);
    }
}

/**
 * @brief Slot for creating new SessionGroups
 *
 */
void Gui::newGroup() {
    std::string groupName =
        QInputDialog::getText(this, "Create new Session Group",
                              "Session Group name:")
            .toStdString();
    if (groupName.length() > 0) {
        // save retrieved data to persistence
        persistence["data"][groupName] = nlohmann::json({});

        // update view data model
        auto item = new QStandardItem(groupName.c_str());
        sessionsData->appendRow(item);
    }
}

/**
 * @brief Slot for editing a Session or SessionGroup, based on the item selected
 *
 */
void Gui::editItem() {
    SelectionData selected = getSelected();

    // selected item is a SessionGroup
    if (selected.itemType == SelectionType::GROUP) {
        std::string groupName =
            QInputDialog::getText(this, "Rename Session Group",
                                  "Session Group name:", QLineEdit::Normal,
                                  selected.itemName.c_str())
                .toStdString();

        if (groupName.length() > 0 &&
            groupName.compare(selected.itemName) != 0) {
            // save retrieved data to persistence
            persistence["data"][groupName] =
                persistence["data"][selected.itemName];
            persistence["data"].erase(selected.itemName);

            // update view data model
            sessionsData->setData(selected.index, groupName.c_str());
        }

        return;
    }

    auto &originalConnection =
        persistence["data"][selected.parentName.c_str()][selected.itemName];

    SessionDialog dialog(this, true);

    dialog.setName(selected.itemName);
    std::string typeString = originalConnection["type"];
    ConnectionType type = typeString.compare("SSH") == 0
                              ? ConnectionType::SSH
                              : ConnectionType::TELNET;
    dialog.setType(type);
    dialog.addGroups(getGroupNames());
    dialog.setGroup(selected.parentName);
    dialog.setIP(originalConnection["ip"]);
    dialog.setUsername(originalConnection["username"]);
    dialog.setPort(originalConnection["port"]);
    int result = dialog.exec();

    if (result == QDialog::Accepted) {
        // save retrieved data to persistence
        auto connection = nlohmann::json({});
        connection["type"] =
            dialog.getType() == ConnectionType::SSH ? "SSH" : "TELNET";
        connection["ip"] = dialog.getIP();
        connection["port"] = dialog.getPort();
        connection["username"] = dialog.getUsername();
        persistence["data"][selected.parentName].erase(selected.itemName);
        persistence["data"][dialog.getGroup()][dialog.getName()] = connection;

        // update view data model
        // Session's group is unchanged
        if (dialog.getGroup().compare(selected.parentName) == 0) {
            sessionsData->setData(selected.index, dialog.getName().c_str(),
                                  Qt::DisplayRole);
            sessionsData->setData(selected.index,
                                  generateConnectionString(connection).c_str(),
                                  Qt::UserRole);
            return;
        }

        // Session's group is changed, need to move it to the target group
        auto old_parent = sessionsData->item(selected.index.parent().row());
        old_parent->removeRow(selected.index.row());

        auto item = new QStandardItem(dialog.getName().c_str());

        auto new_parent = sessionsData->findItems(dialog.getGroup().c_str())[0];
        new_parent->appendRow(item);

        sessionsData->setData(item->index(),
                              generateConnectionString(connection).c_str(),
                              Qt::UserRole);
    }
}

/**
 * @brief Slot for deleting a Session or SessionGroup, based on the item
 * selected
 *
 */
void Gui::deleteItem() {
    SelectionData selected = getSelected();
    QMessageBox mbox(this);
    mbox.setWindowTitle("Delete Item");
    mbox.setText((selected.itemName + " (" +
                  (selected.itemType == SelectionType::GROUP ? "Session Group"
                                                             : "Session") +
                  ") will be deleted.")
                     .c_str());
    mbox.setInformativeText("This action cannot be reverted.");
    mbox.setIcon(QMessageBox::Warning);
    mbox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    int result = mbox.exec();

    if (result == QMessageBox::Ok) {
        switch (selected.itemType) {
        case SelectionType::GROUP:
            persistence["data"].erase(selected.itemName);
            sessionsData->removeRow(selected.index.row());
            break;
        case SelectionType::SESSION:
            sessionsData->item(selected.index.parent().row())
                ->removeRow(selected.index.row());
            persistence["data"][selected.parentName].erase(selected.itemName);
        }
    }
}
/**
 * @brief Retrieves information about the currently selected item
 *
 * @return SelectionData
 */
SelectionData Gui::getSelected() {
    SelectionData data;
    QModelIndex id = sessionsTreeView->currentIndex();
    data.itemType =
        id.parent().isValid() ? SelectionType::SESSION : SelectionType::GROUP;
    data.itemName =
        sessionsTreeView->model()->data(id).toString().toStdString();
    data.parentName = id.parent().isValid() ? sessionsTreeView->model()
                                                  ->data(id.parent())
                                                  .toString()
                                                  .toStdString()
                                            : "";
    data.index = id;
    return data;
}

/**
 * @brief Retrieves a list of all currently existing SessionGroup names
 *
 * @return std::vector<std::string>
 */
std::vector<std::string> Gui::getGroupNames() {
    auto root = sessionsData->invisibleRootItem();
    std::vector<std::string> names;
    for (int i = 0; i < root->rowCount(); ++i) {
        names.push_back(root->child(i)->text().toStdString());
    }

    return names;
}

Gui::~Gui() {}
