#include <gtest/gtest.h>

#include <cstdio>
#include <fstream>
#include <sstream>

#include "llog.h"

#define LOG_FILE(ID) CMAKE_CURRENT_BINARY_DIR "/test_" ID ".log"

/**
 * @brief Checking the initialization of logging with preopened FILE
 */
TEST(llog_tests, init_test) {
    using namespace llog;
    LogStatus ret = init(LogLevel::DEBUG, LOG_FILE("init_test"));

    EXPECT_EQ(ret, LogStatus::INITIALIZED);

    ret = init(LogLevel::DEBUG, LOG_FILE("init_test"));
    EXPECT_EQ(ret, LogStatus::INITIALIZED);

    close();
    std::remove(LOG_FILE("init_test"));
}

/**
 * @brief Checking if the logging finishing corect
 */
TEST(llog_tests, close_test) {
    using namespace llog;
    LogStatus ret = init(LogLevel::DEBUG, LOG_FILE("close_test"));
    EXPECT_EQ(ret, LogStatus::INITIALIZED);

    close();
    EXPECT_EQ(status(), LogStatus::UNINITIALIZED);

    close();
    EXPECT_EQ(status(), LogStatus::UNINITIALIZED);

    std::remove(LOG_FILE("close_test"));
}

/**
 * @brief Checking, if logs with C formated strings works
 */
TEST(llog_tests, printf_test) {
    using namespace llog;
    static constexpr const char *CONTROL_STR0 = "Hello, world! ERROR";
    static constexpr const char *CONTROL_STR1 = "LLOG_LEVEL_WARN 10 10";
    static constexpr const char *CONTROL_STR2 =
        "123456789123456789123456789123456789123456789123456789123456789123"
        "456789123456789123456789123456789123456789123456789123456789123456"
        "789123456789123456789123456789123456789123456789123456789123456789"
        "12345678912345678912rtuiiuhh3456789123456789123456789";

    LogStatus ret = init(LogLevel::DEBUG, LOG_FILE("printf_test"));
    EXPECT_EQ(ret, LogStatus::INITIALIZED);

    error("Hello, world! %s", "ERROR");
    warning("LLOG_LEVEL_WARN %d %d", 10, 10);
    info(CONTROL_STR2);

    close();

    std::ifstream file(LOG_FILE("printf_test"));
    std::stringstream buffer;
    buffer << file.rdbuf();
    std::string str = buffer.str();

    EXPECT_NE(str.find(CONTROL_STR0), std::string::npos);
    EXPECT_NE(str.find(CONTROL_STR1), std::string::npos);
    EXPECT_NE(str.find(CONTROL_STR2), std::string::npos);

    std::remove(LOG_FILE("printf_test"));
}

/**
 * @brief Checking, if the logger is in the corect status
 */
TEST(llog_tests, status_test) {
    using namespace llog;
    EXPECT_EQ(status(), LogStatus::UNINITIALIZED);

    LogStatus ret = init(LogLevel::DEBUG, LOG_FILE("status_test"));

    EXPECT_EQ(ret, LogStatus::INITIALIZED);
    EXPECT_EQ(ret, status());

    close();
    EXPECT_EQ(status(), LogStatus::UNINITIALIZED);

    std::remove(LOG_FILE("status_test"));
}
