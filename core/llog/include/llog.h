#ifndef LLOG_H
#define LLOG_H

#include "llog_util.h"
#include <string>

namespace llog {

/**
 * @brief Used to define the log level.
 */
enum LogLevel { DEBUG, INFO, WARNING, ERROR, NONE };

/**
 * @brief Used to define the log status.
 */
enum LogStatus { UNINITIALIZED, INITIALIZED, CANNOT_OPEN_FILE };

/**
 * @brief Initialize logging
 *
 * @param level
 * @param logFile log file relative path
 * @return LogStatus
 */
LogStatus init(LogLevel level, const std::string &logFile);

/**
 * @brief Logs C formated string.
 * @param file thr file from which the func is called
 * @param line the line from which the func is called
 * @param level The log level on which the message should be printed.
 * @param format C formated string
 * @param ... arguments
 */
void printf(const char *file, const int line, LogLevel level,
            const char *format, ...);

/**
 * @return Current status of the logger.
 */
LogStatus status();

/**
 * @brief Called at the end of a program to finish logging.
 */
void close();
} // namespace llog

/**
 * @brief Logs C formated string in debug level.
 * @param fmt C formated string.
 * @param ... arguments
 */
#define debug(fmt, ...)                                                        \
    printf(__FILENAME__, __LINE__, llog::LogLevel::DEBUG, fmt, ##__VA_ARGS__)

/**
 * @brief Logs C formated string in info level.
 * @param fmt C formated string.
 * @param ... arguments
 */
#define info(fmt, ...)                                                         \
    printf(__FILENAME__, __LINE__, llog::LogLevel::INFO, fmt, ##__VA_ARGS__)

/**
 * @brief Logs C formated string in warning level.
 * @param fmt C formated string.
 * @param ... arguments
 */
#define warning(fmt, ...)                                                      \
    printf(__FILENAME__, __LINE__, llog::LogLevel::WARNING, fmt, ##__VA_ARGS__)

/**
 * @brief Logs C formated string in error level.
 * @param fmt C formated string.
 * @param ... arguments
 */
#define error(fmt, ...)                                                        \
    printf(__FILENAME__, __LINE__, llog::LogLevel::ERROR, fmt, ##__VA_ARGS__)

#endif
