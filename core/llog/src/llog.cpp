#include <cstdarg>
#include <ctime>
#include <fstream>
#include <mutex>

#include <fmt/core.h>
#include <fmt/ostream.h>

#define LLOG_TEST
#include "llog.h"

static constexpr size_t LOG_INIT_LINE_BUFSIZE = 64;

namespace llog {
static std::mutex mtx;
static std::ofstream ofs;

static LogLevel lvl;
static LogStatus stat = LogStatus::UNINITIALIZED;
static char *linebuf;
static size_t linebufSize;

LogStatus init(LogLevel level, const std::string &logFile) {
    if (stat == LogStatus::INITIALIZED)
        return LogStatus::INITIALIZED;

    std::lock_guard<std::mutex> lock(mtx);

    ofs.open(logFile, std::ios::out | std::ios::app);
    if (!ofs.good()) {
        ofs.close();
        return stat = LogStatus::CANNOT_OPEN_FILE;
    }

    linebuf = new char[LOG_INIT_LINE_BUFSIZE];
    linebufSize = LOG_INIT_LINE_BUFSIZE;

    lvl = level;
    return stat = LogStatus::INITIALIZED;
}

void printf(const char *file, const int line, LogLevel level,
            const char *format, ...) {
    if (stat != LogStatus::INITIALIZED || lvl > level ||
        level == LogLevel::NONE)
        return;

    std::lock_guard<std::mutex> lock(mtx);

    static size_t ret;
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);

    va_list args;
printf_retry:
    va_start(args, format);

    ret = vsnprintf(linebuf, linebufSize, format, args);

    if (ret > linebufSize) {
        linebufSize = ret + 1;
        delete[] linebuf;
        linebuf = new char[linebufSize];
        va_end(args);

        goto printf_retry;
    }

    fmt::print(ofs, "{}-{:02}-{:02} {:02}:{:02}:{:02} | {}:{}\t| {}\n",
               tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour,
               tm.tm_min, tm.tm_sec, file, line, linebuf);

    va_end(args);
}

LogStatus status() { return stat; }

void close() {
    if (stat != LogStatus::INITIALIZED)
        return;

    ofs.close();
    delete[] linebuf;
    stat = LogStatus::UNINITIALIZED;
}
} // namespace llog
