cmake_minimum_required(VERSION 3.0.0)
project(persistence VERSION 1.0.0)

set(SOURCES src/persistence.cpp include/persistence.h)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/include ${CMAKE_CURRENT_SOURCE_DIR}/lib/json/single_include)

add_library(persistence STATIC src/persistence.cpp)
#add_executable(${PROJECT_NAME} ${SOURCES})

target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_LIST_DIR}/include")
target_link_libraries(persistence)

add_subdirectory(test EXCLUDE_FROM_ALL)