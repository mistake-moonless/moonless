#ifndef _PERSISTENCE_H
#define _PERSISTENCE_H

#include <nlohmann/json.hpp>
#include <string>

class Persistence {
private:
  nlohmann::json data;
  std::string _configPath;

public:
  /**
   * @brief Construct a new Persistence object
   * 
   * @param configPath Path to configuration file
   */
  Persistence(std::string configPath  = "./config.json");
  ~Persistence();
  /**
   * @brief Reads all data from file
   * 
   */
  void load() noexcept(false);

  /**
   * @brief Writes all stored data to file
   * 
   */
  void save() noexcept(false);

  /**
   * @brief Data structure access operator
   * 
   * @param key Data key
   * @return nlohmann::json& Data value
   */
  nlohmann::json &operator[](const char *key);
};

#endif //_PERSISTENCE_H
