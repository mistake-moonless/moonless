#ifndef _FILE_EXCEPTIONS_H
#define _FILE_EXCEPTIONS_H

#include <stdexcept>

class FileWriteError : public std::runtime_error {

public:
  FileWriteError(const char *what) : runtime_error(what){};
};

class FileParseError : public std::runtime_error {

public:
  FileParseError(const char *what) : runtime_error(what){};
};

#endif //_FILE_EXCEPTIONS_H