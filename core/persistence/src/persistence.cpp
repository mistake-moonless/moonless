#include "persistence.h"
#include "file_exceptions.h"
#include "nlohmann/json.hpp"
#include <filesystem>
#include <fstream>
#include <iostream>

using json = nlohmann::json;

void Persistence::load() noexcept(false) {
  // open file
  std::ifstream input;
  input.open(_configPath);

  // check if it actually opened
  if (!input.good()) {

    // config file doesn't exist yet (probably)
    input.close();
    return;
  }

  // try parsing the file
  try {
    input >> data;
  } catch (nlohmann::detail::parse_error& ex) {
    input.close();
    throw FileParseError("Error parsing the file");
  }
  input.close();
}

void Persistence::save() noexcept(false) {
  std::ofstream output;

  // open file
  output.open(_configPath);

  // check if it actually opened
  if (!output.good()) {
    output.close();
    throw FileWriteError("Can't write specified file");
  }

  // write data
  output << data;
  output.close();
}

Persistence::Persistence(std::string configPath) {
  _configPath = configPath;
  load();
};

Persistence::~Persistence() {
  // save before quit
  save();
};

json &Persistence::operator[](const char *key) { return data[key]; };
