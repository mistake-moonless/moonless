#include <fstream>

#include <gtest/gtest.h>

#include "file_exceptions.h"
#include "persistence.h"

/**
 * @brief Checking the Persistence constuctor
 */
TEST(persistence_test, constructor_test) {
    std::string pathToTestFile =
        std::filesystem::temp_directory_path().string() + "/moonless.json";
    std::ofstream file(pathToTestFile);
    std::string test_content =
        "{\"test_group\": {\"test_session1\": {\"test_name\" : "
        "\"name\",\"test_user_name\" : \"user\",\"test_ip\": "
        "\"192.168.0.1\",\"test_type\": \"telnet\"}}}";

    file << test_content;
    file.close();
    Persistence p(pathToTestFile);

    std::string s = p["test_group"]["test_session1"]["test_name"];

    EXPECT_EQ(s, "name");
}

/**
 * @brief Checking the Persistence constuctor with a non existing file
 */
TEST(persistence_test, constructor_without_existing_file) {
    std::string pathToTestFile =
        std::filesystem::temp_directory_path().string() + "/test.json";

    Persistence p(pathToTestFile);

    p["home"]["study"]["name"] = "Michael";
    p["home"]["study"]["user_name"] = "Red_devil";
    p["home"]["study"]["ip"] = "192.168.0.1";
    p["home"]["study"]["type"] = "ssh";

    p.save();
    p.load();

    std::string s = p["home"]["study"]["name"];

    EXPECT_EQ(s, "Michael");
}

/**
 * @brief Checking, if the save and load methods work correct
 */
TEST(persistence_test, save_load_tests) {
    std::string pathToTestFile =
        std::filesystem::temp_directory_path().string() + "/moonless.json";

    Persistence p(pathToTestFile);

    p["test_group"]["test_session2"]["name"] = "Steven";
    p["test_group"]["test_session2"]["user_name"] = "LSK";
    p["test_group"]["test_session2"]["ip"] = "143";
    p["test_group"]["test_session2"]["type"] = "ssh";

    p.save();
    p.load();

    std::string s = p["test_group"]["test_session2"]["type"];
    std::string r = p["test_group"]["test_session1"]["test_type"];

    EXPECT_EQ(s, "ssh");
    EXPECT_EQ(r, "telnet");

    p["test_group"]["test_session2"].erase("user_name");

    p.save();
    p.load();

    p["test_group"]["test_session2"]["user_name"] = "Little";

    p.save();
    p.load();

    std::string q = p["test_group"]["test_session2"]["user_name"];

    EXPECT_FALSE(q == "LKS");
    EXPECT_EQ(q, "Little");
}