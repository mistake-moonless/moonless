#include <gtest/gtest.h>

#include "connection_type.h"
#include "session.h"
#include "session_group.h"

/**
 * @brief Checking the Session constuctors
 */
TEST(model_tests, session_constructor_basic) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);

    EXPECT_EQ(s.getName(), "name");
    EXPECT_EQ(s.getUserName(), "user");
    EXPECT_EQ(s.getIP(), "ip");
    EXPECT_EQ(s.getPort(), "port");
    EXPECT_EQ(s.getType(), ConnectionType::TELNET);
}

/**
 * @brief Checking, if the overloaded equal with Sessions operator works
 */
TEST(model_tests, session_equal_operator) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name", "user", "ip", "port", ConnectionType::TELNET);
    Session v("name1", "user", "ip", "port", ConnectionType::TELNET);
    Session f("a", "s", "d", "f", ConnectionType::SSH);

    EXPECT_TRUE(s == r);
    EXPECT_TRUE(r == s);
    EXPECT_TRUE(s == s);
    EXPECT_FALSE(s == v);
    EXPECT_FALSE(s == f);
}

/**
 * @brief Checking, if the overloaded not equal operator with Sessions works
 */
TEST(model_tests, session_not_equal_operator) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name", "user", "ip", "port", ConnectionType::TELNET);
    Session v("name1", "user", "ip", "port", ConnectionType::TELNET);
    Session l("name1", "user", "ip", "port", ConnectionType::TELNET);
    Session f("a", "s", "d", "f", ConnectionType::SSH);

    EXPECT_TRUE(s != v);
    EXPECT_TRUE(r != f);
    EXPECT_FALSE(s != r);
    EXPECT_FALSE(v != l);
    EXPECT_FALSE(s != s);
}

/**
 * @brief Checking, if the Session getters work corect
 * (getName, getUserName, getIp, getPort, getType)
 */
TEST(model_tests, session_getters) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name_test", "user_test", "ip_test", "port_test",
              ConnectionType::SSH);

    EXPECT_FALSE(s.getName() == r.getName());
    EXPECT_EQ(s.getUserName(), "user");
    EXPECT_EQ(r.getUserName(), "user_test");
    EXPECT_EQ(s.getIP(), "ip");
    EXPECT_EQ(r.getIP(), "ip_test");
    EXPECT_EQ(s.getPort(), "port");
    EXPECT_EQ(r.getPort(), "port_test");
    EXPECT_FALSE(s.getType() == r.getType());
}

/**
 * @brief Checking the SessionGroup constuctors
 */
TEST(model_tests, session_group_basic_constructors) {
    SessionGroup sg("session_group");

    EXPECT_EQ(sg.getName(), "session_group");
    EXPECT_EQ(sg.size(), 0);

    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);

    std::vector<Session> sessions;
    sessions.push_back(s);
    sessions.push_back(r);

    SessionGroup rg("session_group", sessions);

    EXPECT_EQ(rg.getName(), "session_group");
    EXPECT_EQ(rg.size(), 2);
}

/**
 * @brief Checking, if we could add Sessions to SessionGroup
 */
TEST(model_tests, session_group_add_session) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    std::vector<Session> sessions;

    SessionGroup sg("session_group", sessions);

    EXPECT_EQ(sg.size(), 0);

    sg.addSession(s);

    EXPECT_EQ(sg.size(), 1);

    for (int i = 0; i < 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    EXPECT_EQ(sg.size(), 11);
}

/**
 * @brief Checking, if we remove the corect Sessions from SessionGroup
 */
TEST(model_tests, session_group_remove_session) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    std::vector<Session> sessions;

    SessionGroup sg("session_group", sessions);

    for (int i = 1; i <= 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    EXPECT_EQ(sg.size(), 10);
    EXPECT_EQ(sg[2], s);

    sg.removeSession(2);
    EXPECT_EQ(sg.size(), 9);
    EXPECT_EQ(sg[2], r);

    sg.removeSession(0);
    EXPECT_EQ(sg.size(), 8);
    EXPECT_EQ(sg[0], r);

    sg.removeSession(6);
    EXPECT_EQ(sg.size(), 7);
    EXPECT_EQ(sg[6], r);
}

/**
 * @brief Checking, if the overloaded array index operator gives back the corect
 * Session(normal Session, const Session)
 */
TEST(model_tests, session_group_array_index_operator) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    std::vector<Session> sessions;

    SessionGroup sg("session_group", sessions);

    for (int i = 1; i <= 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    EXPECT_EQ(sg[1].getName(), "name1");
    EXPECT_EQ(sg[1].getUserName(), "user1");
    EXPECT_EQ(sg[1].getIP(), "ip1");
    EXPECT_EQ(sg[1].getPort(), "port1");
    EXPECT_EQ(sg[1].getType(), ConnectionType::SSH);

    Session v = sg[4];

    EXPECT_EQ(v.getName(), "name");
    EXPECT_EQ(v.getUserName(), "user");
    EXPECT_EQ(v.getIP(), "ip");
    EXPECT_EQ(v.getPort(), "port");
    EXPECT_EQ(v.getType(), ConnectionType::TELNET);
}

TEST(model_tests, session_group_array_index_operator_with_const_value) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    std::vector<Session> sessions;

    SessionGroup sg("session_group", sessions);

    for (int i = 1; i <= 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    const Session q = sg[1];

    EXPECT_EQ(q.getName(), "name1");
    EXPECT_EQ(q.getUserName(), "user1");
    EXPECT_EQ(q.getIP(), "ip1");
    EXPECT_EQ(q.getPort(), "port1");
    EXPECT_EQ(q.getType(), ConnectionType::SSH);

    const Session v = sg[4];

    EXPECT_EQ(v.getName(), "name");
    EXPECT_EQ(v.getUserName(), "user");
    EXPECT_EQ(v.getIP(), "ip");
    EXPECT_EQ(v.getPort(), "port");
    EXPECT_EQ(v.getType(), ConnectionType::TELNET);
}

/**
 * @brief Checking, if we can use iterator in foreach loop(normal iterator,
 * const iterator)
 */
TEST(model_tests, session_group_iterator_test) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    SessionGroup sg("session_group");
    std::vector<std::string> names;

    for (int i = 1; i <= 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    for (auto v : sg) {
        names.push_back(v.getName());
    }

    for (size_t i = 0; i < names.size(); i++) {
        if (i % 2 == 0) {
            EXPECT_EQ(names[i], "name");
        } else {
            EXPECT_EQ(names[i], "name1");
        }
    }
}

TEST(model_tests, session_group_const_iterator_test) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    SessionGroup sg("session_group");
    std::vector<std::string> names;

    for (int i = 1; i <= 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    for (const auto &v : sg) {
        names.push_back(v.getName());
    }

    for (size_t i = 0; i < names.size(); i++) {
        if (i % 2 == 0) {
            EXPECT_EQ(names[i], "name");
        } else {
            EXPECT_EQ(names[i], "name1");
        }
    }
}

/**
 * @brief Checking, if the size and empty methods of SessionGroup works corect
 */
TEST(model_tests, session_group_empty_and_size_method) {
    Session s("name", "user", "ip", "port", ConnectionType::TELNET);
    Session r("name1", "user1", "ip1", "port1", ConnectionType::SSH);
    SessionGroup sg("session_group");

    EXPECT_TRUE(sg.empty());
    EXPECT_EQ(sg.size(), 0);

    sg.addSession(s);

    EXPECT_FALSE(sg.empty());
    EXPECT_EQ(sg.size(), 1);

    for (int i = 1; i <= 5; i++) {
        sg.addSession(s);
        sg.addSession(r);
    }

    EXPECT_EQ(sg.size(), 11);

    sg.removeSession(0);
    sg.removeSession(5);
    sg.removeSession(7);

    EXPECT_FALSE(sg.empty());
    EXPECT_EQ(sg.size(), 8);
}

/**
 * @brief Checking, if we can set a name to SessionGroup
 */
TEST(model_tests, session_group_set_name_method) {
    SessionGroup sg("session_group");
    SessionGroup rg(sg);

    EXPECT_EQ(sg.getName(), "session_group");
    EXPECT_EQ(rg.getName(), "session_group");

    sg.setName("test");
    rg.setName("asd");

    EXPECT_EQ(sg.getName(), "test");
    EXPECT_EQ(rg.getName(), "asd");
}
