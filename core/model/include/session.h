#ifndef _SESSION_H_
#define _SESSION_H_

#include <array>
#include <string>

#include "connection_type.h"

class Session {
  private:
    std::string name;
    ConnectionType type;

    // [0]: userName, [1]: ip, [2]: port
    std::array<std::string, 3> data;

  public:
    /**
     * @brief Construct a new Session object
     *
     * @param name name of the session
     * @param userName user name used to login to the session, can be empty
     * @param ip ip of the target session
     * @param port port of the target session
     * @param type communication type, telnet or ssh
     */
    Session(const std::string &name, const std::string &userName,
            const std::string &ip, const std::string &port,
            ConnectionType type);

    bool operator==(const Session &) const;
    bool operator!=(const Session &) const;

    /**
     * @brief Get the name of the session
     */
    const std::string &getName() const noexcept;

    /**
     * @brief Get the user name used to login to the session
     */
    const std::string &getUserName() const noexcept;

    /**
     * @brief Get the ip of the target session
     */
    const std::string &getIP() const noexcept;

    /**
     * @brief Get the port of the target session
     */
    const std::string &getPort() const noexcept;

    /**
     * @brief Get the communication type
     */
    ConnectionType getType() const noexcept;
};

#endif
