#ifndef _SESSION_GROUP_H_
#define _SESSION_GROUP_H_

#include "connection_type.h"
#include "session.h"
#include <vector>

class SessionGroup {
  private:
    std::string name;
    std::vector<Session> sessions;

    using iterator = std::vector<Session>::iterator;
    using const_iterator = std::vector<Session>::const_iterator;

  public:
    /**
     * @brief Construct a new SessionGroup object
     */
    SessionGroup(const std::string &name);

    /**
     * @brief Construct a new SessionGroup object
     */
    SessionGroup(const std::string &name, const std::vector<Session> &sessions);

    void setName(const std::string &);
    const std::string &getName() const noexcept;

    /**
     * @brief Add a new session at the end of the group
     */
    void addSession(const Session &);

    /**
     * @brief Remove a session from the group by index
     */
    void removeSession(size_t idx);

    Session &operator[](size_t);
    const Session &operator[](size_t) const;

    iterator begin() noexcept;
    const_iterator begin() const noexcept;
    const_iterator cbegin() const noexcept;

    iterator end() noexcept;
    const_iterator end() const noexcept;
    const_iterator cend() const noexcept;

    /**
     * @brief Number of sessions in the group
     */
    size_t size() const noexcept;

    /**
     * @brief Check if the group is empty
     */
    bool empty() const noexcept;
};

#endif
