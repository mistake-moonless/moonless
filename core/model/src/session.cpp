#include <array>
#include <string>

#include "session.h"

Session::Session(const std::string &name, const std::string &userName,
                 const std::string &ip, const std::string &port,
                 ConnectionType type)
    : name(name), type(type) {
    data = {userName, ip, port};
}

bool Session::operator==(const Session &other) const {
    return this->type == other.type && this->name == other.name &&
           data[0] == other.data[0] && data[1] == other.data[1] &&
           data[2] == other.data[2];
}
bool Session::operator!=(const Session &other) const {
    return !(*this == other);
}

const std::string &Session::getName() const noexcept { return name; }

const std::string &Session::getUserName() const noexcept { return data[0]; }

const std::string &Session::getIP() const noexcept { return data[1]; }

const std::string &Session::getPort() const noexcept { return data[2]; }

ConnectionType Session::getType() const noexcept { return type; }
