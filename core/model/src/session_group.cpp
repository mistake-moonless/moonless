#include <string>
#include <vector>

#include "session.h"
#include "session_group.h"

SessionGroup::SessionGroup(const std::string &name) : name(name), sessions() {}

SessionGroup::SessionGroup(const std::string &name,
                           const std::vector<Session> &sessions)
    : name(name), sessions(sessions) {}

void SessionGroup::setName(const std::string &name) { this->name = name; }

const std::string &SessionGroup::getName() const noexcept { return name; }

void SessionGroup::addSession(const Session &session) {
    sessions.push_back(session);
}

void SessionGroup::removeSession(size_t idx) {
    sessions.erase(sessions.begin() + idx);
}

Session &SessionGroup::operator[](size_t idx) { return sessions[idx]; }

const Session &SessionGroup::operator[](size_t idx) const {
    return const_cast<SessionGroup *>(this)->operator[](idx);
}

SessionGroup::iterator SessionGroup::begin() noexcept {
    return sessions.begin();
}

SessionGroup::const_iterator SessionGroup::begin() const noexcept {
    return sessions.begin();
}

SessionGroup::const_iterator SessionGroup::cbegin() const noexcept {
    return sessions.cbegin();
}

SessionGroup::iterator SessionGroup::end() noexcept { return sessions.end(); }

SessionGroup::const_iterator SessionGroup::end() const noexcept {
    return sessions.end();
}

SessionGroup::const_iterator SessionGroup::cend() const noexcept {
    return sessions.cend();
}

size_t SessionGroup::size() const noexcept { return sessions.size(); }

bool SessionGroup::empty() const noexcept { return sessions.empty(); }
