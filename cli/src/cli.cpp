#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <stdexcept>
#include <string>

#include "cli.h"
#include "connection_type.h"
#include "llog.h"
#include "nlohmann/json.hpp"
#include "session.h"
#include "session_group.h"

static bool isNumber(const std::string &str) {
    for (const char &c : str) {
        if (std::isdigit(c) == 0)
            return false;
    }
    return true;
}

static void logo() {
    fmt::print(fmt::emphasis::bold | fg(fmt::color::orange),
               "\nWelcome to Moonless:\n");
    fmt::print(
        fg(fmt::color::orange),
        "                                       ************.\n"
        "                        #@@@*                               @@@@\n"
        "                  @@&                                             "
        "  "
        " .@@#\n"
        "             @@                                  ...              "
        "  "
        "        @@\n"
        "         @@                                     *   / //          "
        "  "
        "            @@\n"
        "      &@                                ,         ///////,        "
        "  "
        "               @/\n"
        "    @                                  /,      ///////////        "
        "  "
        "                 /@\n"
        "  @,                                  /// ////////////////        "
        "  "
        "                   @#\n"
        " @                                    ///// */////////////        "
        "  "
        "                    *@\n"
        "@(                                    ///////      ///////////    "
        "  "
        "                     &.\n"
        "@                                      //////          ///////    "
        "  "
        "                      .\n"
        "@/                                      *//////       ///////     "
        "  "
        "                     &.\n"
        " @                                         ////    ///////,       "
        "  "
        "                    *@\n"
        "  @,                                            #////             "
        "  "
        "                   @#\n"
        "    @                                                             "
        "  "
        "                 *@\n"
        "      &@                                                          "
        "  "
        "               @(\n"
        "         @@                                                       "
        "  "
        "            @@\n"
        "             @@                                                   "
        "  "
        "        @@\n"
        "                  @@%                                             "
        "  "
        " .@@%\n"
        "                         #@@@*                               "
        "@@@@.\n"
        "                                       ////////////,\n            "
        "  "
        "                                       ");
}

void Cli::menu() {
    if (firstTimeSetup) {
        logo();
    }

    while (true) {
        fmt::print(fg(fmt::color::orange),
                   "\nPlease Select from the following options.\n q:Quit\n "
                   "a:Add\n r:Remove\n ");
        try {
            auto obj = persistence["data"].get<nlohmann::json::object_t>();
            int counter = 0;

            for (auto &kvp : obj) {
                fmt::print(fg(fmt::color::blue), kvp.first);
                fmt::print(fg(fmt::color::blue), ":\n");

                for (auto &it : kvp.second.get<nlohmann::json::object_t>()) {
                    std::string strct = "\t";
                    strct += std::to_string(counter) + ":";
                    strct += it.first + ": ";
                    strct += "ip: ";
                    strct += persistence["data"][kvp.first][it.first]["ip"];
                    strct += " port: ";
                    strct += persistence["data"][kvp.first][it.first]["port"];
                    strct += " type: ";
                    strct += persistence["data"][kvp.first][it.first]["type"];
                    strct += " userName: ";
                    strct +=
                        persistence["data"][kvp.first][it.first]["userName"];
                    strct += "\n";

                    fmt::print(fg(fmt::color::light_blue), strct);
                    counter++;
                }
            }
        } catch (const std::exception &e) {
        }

        std::string s;
        std::cin >> s;
        if (s == "a") {
            llog::info("Started an add operation.");
            add();
        } else if (s == "r") {
            llog::info("Started a remove operation.");
            remove();
        } else if (s == "q") {
            llog::info("Quiting the program.");
            llog::close();
            break;
        } else if ((isNumber(s) && stoi(s) >= 0) || s.length() > 1) {
            auto obj = persistence["data"].get<nlohmann::json::object_t>();
            int counter = 0;
            for (auto &kvp : obj) {
                if (kvp.first == s) {
                    const char *typetmp = kvp.first.c_str();
                    llog::info("Starting a sessionGroup with group %s",
                               typetmp);
                    connectGroup(kvp);
                }
                for (auto &it : kvp.second.get<nlohmann::json::object_t>()) {
                    if (isNumber(s) && counter == stoi(s)) {
                        const char *typetmp = it.first.c_str();
                        llog::info("Starting a sessionGroup with group %s",
                                   typetmp);
                        connect(kvp.first, it.first);
                    }
                    counter++;
                }
            }
        } else {
            fmt::print(
                fg(fmt::color::red),
                "The menu point you selected does not exist pleasy try again");
        }
    }
}

void Cli::add() {
    std::string ip, group, name, userName, port, type;
    bool error = true;

    fmt::print(fg(fmt::color::orange),
               "Please Write the group you would like to assign this adress "
               "the name of the adress and the ip you would like to "
               "join.(group name ip port username connectiontype)\n");

    while (error) {
        std::cin >> group >> name >> ip >> port >> userName >> type;
        error = false;
        std::string errors;

        if (!(type == "SSH" || type == "ssh" || type == "telnet" ||
              type == "TELNET")) {
            const char *typetmp = type.c_str();
            llog::warning("Wrong connection type was given: %s", typetmp);
            errors += "You given the wrong connection type.\n";
            error = true;
        }
        try {
            if (stoi(port) < 0 || stoi(port) > 65536) {
                llog::warning("Wrong port number was given: %d", stoi(port));
                errors += "You given the wrong port number.\n";
                error = true;
            }
        } catch (std::invalid_argument e) {
            errors += "The port you have given is of wrong type\n";
            error = true;
        }

        if (errors.length() != 0)
            fmt::print(fg(fmt::color::red), errors + "Please try again\n");
    }
    char *c_group = &group[0];
    char *c_name = &name[0];

    llog::info("Data was written to persistence into group:%s  under name:%s",
               c_group, c_name);

    persistence["data"][c_group][c_name]["ip"] = ip;
    persistence["data"][c_group][c_name]["userName"] = userName;
    persistence["data"][c_group][c_name]["port"] = port;
    persistence["data"][c_group][c_name]["type"] = type;
    fmt::print(fg(fmt::color::orange), "\n");
}

void Cli::remove() {
    std::string ip, group, name;
    fmt::print(fg(fmt::color::orange),
               "Please Write the group and name of the the addres you would "
               "like to remove(group name)\n");

    std::cin >> group >> name;
    char *c_group = &group[0];
    char *c_name = &name[0];

    llog::info("Data was removed from persistence from group:%s  under name:%s",
               c_group, c_name);

    persistence["data"][c_group].erase(c_name);
    if (persistence["data"][c_group].size() == 0) {
        persistence["data"].erase(c_group);
    }

    fmt::print(fg(fmt::color::red),
               "The record of " + name + " has been deleted\n");
}

Session Cli::connect(std::string groupName, std::string name) {
    char *c_group = &groupName[0];
    char *c_name = &name[0];

    if (persistence["data"][c_group][c_name]["type"] == "SSH" ||
        persistence["data"][c_group][c_name]["type"] == "ssh") {
        Session s(name, persistence["data"][c_group][c_name]["userName"],
                  persistence["data"][c_group][c_name]["ip"],
                  persistence["data"][c_group][c_name]["port"], SSH);
        std::string command = "";

        command += "ssh ";
        command += "" + s.getIP();
        command += " -p " + s.getPort();
        command += " -l " + s.getUserName();
        command += " -o ConnectTimeout=3";
        llog::info("Connecting to the following  %s", &command[0]);
        std::system(&command[0]);
        return s;
    }

    Session s(name, persistence["data"][c_group][c_name]["userName"],
              persistence["data"][c_group][c_name]["ip"],
              persistence["data"][c_group][c_name]["port"], TELNET);
    std::string command = "";

    command += "telnet ";
    command += " -l " + s.getUserName();
    command += " " + s.getIP();
    command += " " + s.getPort();
    llog::info("Connecting to the following  %s", &command[0]);
    std::system(&command[0]);

    return s;
}

void Cli::connectGroup(
    std::pair<const std::basic_string<char>, nlohmann::basic_json<>> &data) {
    SessionGroup sg(data.first);
    for (auto &it : data.second.get<nlohmann::json::object_t>()) {
        sg.addSession(connect(data.first, it.first));
    }
}

static std::string getSaveFilePath() {
#if _WIN32
    return std::string(std::getenv("HOMEDRIVE")) +
           std::string(std::getenv("HOMEPATH")) + "\\.moonless_cli.json";
#elif __linux__
    return std::string(std::getenv("HOME")) + "/.moonless_cli.json";
#else
#error Not supported platform
#endif
}

Cli::Cli() : firstTimeSetup(true) {
    ret = init(llog::LogLevel::INFO, TEMP_FILE);
    persistence = Persistence(getSaveFilePath());
}

int main() {
    Cli cli;
    cli.menu();
}
