#ifndef CLI_H
#define CLI_H
#include <fmt/color.h>

#include "llog.h"
#include "persistence.h"
#include "session.h"

#if _WIN32
#define TEMP_FILE "C:\\Temp\\moonless_cli.log"
#elif __linux__
#define TEMP_FILE "/var/tmp/moonless_cli.log"
#else
#error TEMP_FILE is not set
#endif

class Cli {
  public:
    Cli();

    /**
     * @brief Displays main menu
     */
    void menu();

    /**
     * @brief Adds a new connection to it's group in the persistence
     */
    void add();

    /**
     * @brief Removes connections or groups of connection from the Persistence
     */
    void remove();

    /**
     * @brief Quits the menu
     */
    void quit();

    /**
     * @brief Handles the individual connections
     *
     * @param groupName The name of the group in which the connection is found
     * @param name Name of the specific connection
     * @return Session
     */
    Session connect(std::string groupName, std::string name);

    /**
     * @brief Handles group connection
     * @param data Json of the connections within the group
     */
    void connectGroup(
        std::pair<const std::basic_string<char>, nlohmann::basic_json<>> &data);

  private:
    llog::LogStatus ret;
    bool firstTimeSetup;
    Persistence persistence;
};
#endif
